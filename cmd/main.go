package main

import (
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/konfka/go-rpc-auth/config"
	"gitlab.com/konfka/go-rpc-auth/internal/infrastructure/logs"
	"gitlab.com/konfka/go-rpc-auth/run"
	"log"
	"os"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}

	conf := config.NewAppConf()

	logger := logs.NewLogger(conf, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}
	// Инициализируем конфигурацию приложения с логгером
	conf.Init(logger)
	// Создаем инстанс приложения
	app := run.NewApp(conf, logger)

	exitCode := app.
		// Инициализируем приложение
		Bootstrap().
		// Запускаем приложение
		Run()
	os.Exit(exitCode)
}
