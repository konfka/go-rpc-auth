package client

import (
	"context"
	"fmt"
	"gitlab.com/konfka/go-rpc-auth/config"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"sync"
)

type ClientGRPC interface {
	//Call(serviceMethod string, args any, reply any) error
}

type ClientJSONGRPC struct {
	conf   config.RPCClient
	logger *zap.Logger
	client *grpc.ClientConn
	sync.Mutex
}

func (c *ClientJSONGRPC) Invoke(ctx context.Context, method string, args interface{}, reply interface{}, opts ...grpc.CallOption) error {
	return c.client.Invoke(ctx, method, args, reply, opts...)
}

func (c *ClientJSONGRPC) NewStream(ctx context.Context, desc *grpc.StreamDesc, method string, opts ...grpc.CallOption) (grpc.ClientStream, error) {
	return c.client.NewStream(ctx, desc, method, opts...)
}

func NewJSONGRPC(conf config.RPCClient, logger *zap.Logger) *ClientJSONGRPC {
	cleint, err := grpc.Dial(fmt.Sprintf("%s:%s", conf.Host, conf.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		logger.Fatal("grpc not connection")
	}
	logger.Info("grpc connection")
	return &ClientJSONGRPC{conf: conf, logger: logger, client: cleint}
}
