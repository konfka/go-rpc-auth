package client

import (
	"fmt"
	"gitlab.com/konfka/go-rpc-auth/config"
	"go.uber.org/zap"
	"net/rpc"
	"net/rpc/jsonrpc"
	"sync"
)

type Client interface {
	Call(serviceMethod string, args any, reply any) error
}

type ClientJSONRPC struct {
	conf   config.RPCClient
	logger *zap.Logger
	client *rpc.Client
	sync.Mutex
}

func (c *ClientJSONRPC) Call(serviceMethod string, args any, reply any) error {
	var err error

	// если подключение нет, создаем его
	if c.client == nil {
		c.Lock()
		c.client, err = jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", c.conf.Host, c.conf.Port))
		if err != nil {
			c.logger.Error("error init rpc client", zap.Error(err))
			return err
		}
		c.Unlock()

	}

	return c.client.Call(serviceMethod, args, reply)
}

func NewJSONRPC(conf config.RPCClient, logger *zap.Logger) Client {
	// попытка проинициализировать rpc клиента при старте приложения
	client, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", conf.Host, conf.Port))
	if err != nil {
		client = nil
	}

	return &ClientJSONRPC{conf: conf, client: client, logger: logger}
}
