package server

import (
	"context"
	"fmt"
	"gitlab.com/konfka/go-rpc-auth/config"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"net"
)

type GRPCServer struct {
	conf   config.RPCServer
	logger *zap.Logger
	srv    *grpc.Server
}

func NewGRPCServer(conf config.RPCServer, srv *grpc.Server, logger *zap.Logger) Server {
	return &GRPCServer{conf: conf, logger: logger, srv: srv}
}

func (s *GRPCServer) Serve(ctx context.Context) error {
	var err error

	var l net.Listener
	l, err = net.Listen("tcp", fmt.Sprintf(":%s", s.conf.Port))
	if err != nil {
		s.logger.Error("grpc server register error", zap.Error(err))
	}

	s.logger.Info("grpc server started", zap.String("port", s.conf.Port))

	err = s.srv.Serve(l)

	return err
}
