package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/konfka/go-rpc-auth/internal/infrastructure/component"
	"gitlab.com/konfka/go-rpc-auth/internal/infrastructure/errors"
	"gitlab.com/konfka/go-rpc-auth/internal/infrastructure/handlers"
	"gitlab.com/konfka/go-rpc-auth/internal/infrastructure/responder"
	"gitlab.com/konfka/go-rpc-auth/internal/modules/user/service"
	"gitlab.com/konfka/go-rpc-auth/user_ext/controller"
	"net/http"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, controller.ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: controller.Data{
				Message: "retrieving auth error",
			},
		})
		return
	}

	u.OutputJSON(w, controller.ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: controller.Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}
