package modules

import (
	"gitlab.com/konfka/go-rpc-auth/internal/infrastructure/client"
	"gitlab.com/konfka/go-rpc-auth/internal/infrastructure/component"
	aservice "gitlab.com/konfka/go-rpc-auth/internal/modules/auth/service"
	"gitlab.com/konfka/go-rpc-auth/internal/modules/user/service"
	uservice "gitlab.com/konfka/go-rpc-auth/internal/modules/user/service"
	"gitlab.com/konfka/go-rpc-auth/internal/storages"
	"gitlab.com/konfka/go-rpc-auth/rpc/grpc/proto_user"
)

type Services struct {
	User service.Userer
	Auth aservice.Auther
}

func NewServiceJRPC(storages *storages.Storages, components *component.Components) *Services {
	userRPC := client.NewJSONRPC(components.Conf.UserRPC, components.Logger)
	userServiceRPC := uservice.NewUserServiceJSONRPC(userRPC)
	return &Services{
		User: userServiceRPC,
		Auth: aservice.NewAuth(userServiceRPC, storages.Verify, components),
	}
}

func NewServicesGRPS(storages *storages.Storages, components *component.Components) *Services {
	userGRPS := client.NewJSONGRPC(components.Conf.UserRPC, components.Logger)
	userGRPSClient := proto_user.NewUserServiceRPCClient(userGRPS)
	userService := uservice.NewExchangeServiceGRPC(userGRPSClient)

	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
