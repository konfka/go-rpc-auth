package storages

import (
	"gitlab.com/konfka/go-rpc-auth/internal/db/adapter"
	vstorage "gitlab.com/konfka/go-rpc-auth/internal/modules/auth/storage"
)

type Storages struct {
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
