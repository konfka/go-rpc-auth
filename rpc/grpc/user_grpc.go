package grpc

import (
	"context"
	"gitlab.com/konfka/go-rpc-auth/internal/infrastructure/errors"
	"gitlab.com/konfka/go-rpc-auth/internal/modules/user/service"
	"gitlab.com/konfka/go-rpc-auth/rpc/grpc/proto_user"
)

type UserServiceGRPC struct {
	client proto_user.UserServiceRPCClient
}

func NewUserServiceGRPC(client proto_user.UserServiceRPCClient) *UserServiceGRPC {
	return &UserServiceGRPC{client: client}
}

func (u *UserServiceGRPC) Create(ctx context.Context, in service.UserCreateIn) service.UserCreateOut {
	req, err := u.client.Create(ctx, &proto_user.UserCreateIn{
		Name:     in.Name,
		Phone:    in.Phone,
		Email:    in.Email,
		Password: in.Password,
		Role:     int32(in.Role),
	})
	if err != nil {
		return service.UserCreateOut{
			UserID:    0,
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}
	return service.UserCreateOut{
		UserID:    int(req.GetUserId()),
		ErrorCode: int(req.GetErrorCode()),
	}
}
